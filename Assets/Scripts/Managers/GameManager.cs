﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance = null;

    public bool gameIsOn = false;
    public bool ballLaunched = false;

    public GameObject ballPrefab;
    Ball ball;

    public Paddle paddle;
    public Transform paddleStartPosition;

    public long numberOfSessions = 10;

    public Text gameScoreText;
    public long gameScore = 0;

    public Text topScoreText;
    public long topScore = 0;

    public static Color[] brickColors2;
    public Color[] brickColors;

    public List<Brick> bricks = new List<Brick>();

    public Canvas menuCanvas;

    public delegate void GameEvent();

    public event GameEvent gamePaused;
    public event GameEvent gameResumed;
    public event GameEvent gameStarted;
    public event GameEvent gameEnded;

    void Awake()
    {
        if (Instance)
            Destroy(this);
        else
            Instance = this;

        topScore = PlayerPrefsX.GetLong("TopScore");

        WriteScore();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) && !ballLaunched)
        {
            LaunchBall();
        }
    }

    public void StartGame()
    {
        if(ball == null)
            CreateBallClone(ballPrefab);

        ResetGame();

        menuCanvas.gameObject.SetActive(false);

        if (gameStarted != null)
            gameStarted();

        gameIsOn = true;
    }

    public void ResetGame()
    {
        paddle.transform.position = paddleStartPosition.position;
        paddle.TakeBall(ball);

        foreach (Brick brick in bricks)
        {
            brick.Reset();
        }
    }

    void CreateBallClone(GameObject prefab)
    {
        GameObject ballClone = Instantiate(prefab);

        ball = ballClone.GetComponent<Ball>();

        paddle.TakeBall(ball);
    }

    public void LaunchBall()
    {
        if (ball)
        {
            paddle.LaunchBall();

            ballLaunched = true;
        }
    }

    void WriteScore()
    {
        topScore = gameScore > topScore ? gameScore : topScore;

        gameScoreText.text = "SCORE: " + gameScore;
        topScoreText.text = "RECORD: " + topScore;

        PlayerPrefsX.SetLong("TopScore", topScore);
    }

    public void EndGame()
    {
        menuCanvas.gameObject.SetActive(true);

        WriteScore();

        gameScore = 0;

        if(gameEnded != null)
            gameEnded();

        ballLaunched = false;
        gameIsOn = false;
    }

    public void PauseGame()
    {
        if (gamePaused != null)
            gamePaused();

        gameIsOn = false;
    }

    public void PauseResumed()
    {
        if (gameResumed != null)
            gameResumed();

        gameIsOn = true;
    }

    public void ExitGame()
    {
        Debug.Log("Exiting");
        Application.Quit();
    }
}
