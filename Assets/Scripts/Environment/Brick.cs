﻿using System.Collections;
using UnityEngine;

public class Brick : MonoBehaviour
{
    public GameManager gameManager;

    public int scorePerDurability = 10;

    public int durability = 3;
	public int maxDurability;

	public float destructionDelay = 1f;

    public bool indestructible = false;

    public float animationDuration = 0.5f;

    SpriteRenderer spriteRenderer;
    Collider2D ownCollider;
    public Material material;

    void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        ownCollider = GetComponent<Collider2D>();

        material = spriteRenderer.material;

        GameManager.Instance.bricks.Add(this);
    }

	#if UNITY_EDITOR
    public void OnValidate()
    {
        gameManager = GameObject.Find("Game Manager").GetComponent<GameManager>();

        GetComponent<SpriteRenderer>().color = GetColor(maxDurability);
    }
	#endif

    Color GetColor(int index)
    {
        if (indestructible)
        {
            return gameManager.brickColors[0];
        }
        else
        {
            if (index >= 0f && index <= gameManager.brickColors.Length)
                return gameManager.brickColors[index];
        }

        return Color.black;
    }

    public void Reset()
    {
        durability = maxDurability;

        spriteRenderer.color = GetColor(maxDurability);

        spriteRenderer.enabled = true;
        ownCollider.enabled = true;
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        GameManager.Instance.gameScore += durability * scorePerDurability;

        if (indestructible)
        {
            StartCoroutine(ChangeColor(spriteRenderer.color, animationDuration));
        }
        else
        {
            durability--;

            if (durability >= 1)
            {
                StartCoroutine(ChangeColor(GetColor(durability), animationDuration));
            }
            else
            {
                StartCoroutine(Hide(animationDuration));
            }
        }
    }

    IEnumerator Hide(float duration)
    {
        Color color = spriteRenderer.color;

		float progress = 0f;
        float startTime = Time.time;

        while(color.a > 0f)
        {
			progress = (Time.time - startTime) / duration;
            color.a = 1f - progress;

            spriteRenderer.color = color;

            yield return null;
        }

        spriteRenderer.enabled = false;
        ownCollider.enabled = false;
    }

    IEnumerator ChangeColor(Color endColor, float duration)
    {
        Color startColor = spriteRenderer.color;

        float value = 0f;
        float startTime = Time.time;

        while(value < 1f)
        {
            value = (Time.time - startTime) / duration;

            spriteRenderer.color = Color.Lerp(startColor, endColor, value);
            material.SetFloat("_ShineLocation", value);
            material.SetFloat("_ShineWidth", 0.5f - Mathf.Abs(0.5f - value));

            yield return null;
        }

        spriteRenderer.color = endColor;
        material.SetFloat("_ShineLocation", 0f);
        material.SetFloat("_ShineWidth", 0f);
    }
}
