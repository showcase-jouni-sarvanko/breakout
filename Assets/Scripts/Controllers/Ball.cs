﻿using UnityEngine;

public class Ball : MonoBehaviour
{
    #region Public Fields

    public float startSpeed = 3f;
    public float speed = 3f;
    public float collisionAcceleration = 0.01f;
    public float verticalPull = 0.01f;

    #endregion

    #region Private Fields

    Rigidbody2D rbody;
    RigidbodyType2D state;
    Vector3 savedVelocity;
    float savedAngularVelocity;
    
    #endregion

    #region Unity Events

    void Awake()
    {
        rbody = GetComponent<Rigidbody2D>();

        state = rbody.bodyType;
        savedVelocity = rbody.velocity;
        savedAngularVelocity = rbody.angularVelocity;
    }

    void OnEnable()
    {
        GameManager.Instance.gameStarted += OnGameStarted;
        GameManager.Instance.gameEnded += OnGameEnded;

        GameManager.Instance.gamePaused += OnGamePaused;
        GameManager.Instance.gameResumed += OnGameResumed;
    }

    void OnDisable()
    {
        GameManager.Instance.gameStarted -= OnGameStarted;
        GameManager.Instance.gameEnded -= OnGameEnded;

        GameManager.Instance.gamePaused -= OnGamePaused;
        GameManager.Instance.gameResumed -= OnGameResumed;
    }

    void Update ()
    {
        if (GameManager.Instance.gameIsOn)
        {
            AddVerticalPull();
            MaintainSpeed();
        }
    }

    #endregion

    #region Velocity Handling

    void AddVerticalPull()
    {
        rbody.velocity += Vector2.up * rbody.velocity.normalized.y * verticalPull;
    }

    void MaintainSpeed()
    {
         rbody.velocity = rbody.velocity.normalized * speed;
    }

    void AddCollisionAcceleration()
    {
        speed += collisionAcceleration;
    }

    #endregion

    #region Game Action Handling

    public void LaunchBall(Vector2 movementVelocity)
    {
        transform.parent = null;
        rbody.bodyType = RigidbodyType2D.Dynamic;

        speed = startSpeed;

        Vector2 launchVelocity = Vector2.up * speed + movementVelocity;

        rbody.AddForce(launchVelocity * rbody.mass, ForceMode2D.Impulse);
    }

    public void ResetBall(Transform launchPoint)
    {
        transform.parent = launchPoint;
        rbody.bodyType = RigidbodyType2D.Kinematic;

        rbody.velocity = Vector2.zero;
        rbody.angularVelocity = 0f;

        savedVelocity = Vector2.zero;
        savedAngularVelocity = 0f;

        transform.localPosition = Vector2.zero;
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        AddCollisionAcceleration();

        Paddle paddle = collision.collider.gameObject.GetComponent<Paddle>();

        if(paddle)
        {
            Debug.Log("Ball collided with paddle");

            Rigidbody2D rbodyPaddle = paddle.GetComponent<Rigidbody2D>();

            rbody.AddForceAtPosition(rbodyPaddle.velocity * 0.003f, collision.contacts[0].point, ForceMode2D.Impulse);
        }
        else
        {
            Debug.Log("Ball collided with wall");
        }
    }

    #endregion

    #region Game State Handling

    void OnGameStarted()
    {
        rbody.bodyType = RigidbodyType2D.Kinematic;
    }

    void OnGameEnded()
    {
        rbody.bodyType = RigidbodyType2D.Static;
    }

    void OnGamePaused()
    {
        state = rbody.bodyType;
        savedVelocity = rbody.velocity;
        savedAngularVelocity = rbody.angularVelocity;
        rbody.bodyType = RigidbodyType2D.Static;
    }

    void OnGameResumed()
    {
        rbody.bodyType = state;
        rbody.AddForce(savedVelocity * rbody.mass, ForceMode2D.Impulse);
        rbody.AddTorque(savedAngularVelocity * rbody.mass, ForceMode2D.Impulse);
    }

    #endregion
}
