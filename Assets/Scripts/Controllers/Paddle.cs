﻿using UnityEngine;

public class Paddle : MonoBehaviour
{
    public float speed = 3f;
    public float acceleration = 3f;
    public float stopSpeed = 0.1f;
    public float maxDistance = 6.2f;

    public float horizontal = 0f;

    public Ball ball;
    public Transform launchPosition;

    Rigidbody2D rbody;
    Vector3 savedVelocity;

    #region Unity Events

    void Awake()
    {
        rbody = GetComponent<Rigidbody2D>();
    }

    void OnEnable()
    {
        GameManager.Instance.gameStarted += OnGameStarted;
        GameManager.Instance.gameEnded += OnGameEnded;

        GameManager.Instance.gamePaused += OnGameStarted;
        GameManager.Instance.gameResumed += OnGameResumed;
    }

    void OnDisable()
    {
        GameManager.Instance.gameStarted -= OnGameStarted;
        GameManager.Instance.gameEnded -= OnGameEnded;

        GameManager.Instance.gamePaused -= OnGameStarted;
        GameManager.Instance.gameResumed -= OnGameResumed;
    }

    void Update()
    {
        if (GameManager.Instance.gameIsOn)
        {
            horizontal = Input.GetAxis("Horizontal");

            rbody.velocity = Vector3.right * horizontal * speed;
        }
    }

    #endregion

    #region Game Action Handling
    public void LaunchBall()
    {
        ball.LaunchBall(rbody.velocity);
    }

    public void TakeBall(Ball ball)
    {
        this.ball = ball;

        ball.ResetBall(launchPosition);
    }

    #endregion

    #region Game State Handling

    void OnGameStarted()
    {
        rbody.bodyType = RigidbodyType2D.Dynamic;
    }

    void OnGameEnded()
    {
        rbody.bodyType = RigidbodyType2D.Static;
    }

    void OnGamePaused()
    {
        savedVelocity = rbody.velocity;
        rbody.bodyType = RigidbodyType2D.Static;
    }

    void OnGameResumed()
    {
        rbody.bodyType = RigidbodyType2D.Dynamic;
        rbody.AddForce(savedVelocity * rbody.mass, ForceMode2D.Impulse);
    }

    #endregion
}
